'use client'

import Head from 'next/head';
import { initiateCheckout } from '@/libs';
import products from './data/products.json';

export default function Home() {
  return (
    <>
    <Head>
      <title>Space Jelly Shop</title>
    </Head>
      <main className="flex min-h-screen flex-col items-center justify-between p-24">
        <h1 className="text-5xl	font-bold">Space Jelly Shop</h1>
        <h3 className="text-1xl mb-12 mt-8">
          The best space jellyfish swag on the web!
        </h3>
        <div className="mb-32">
          <ul className="grid gap-10 lg:mb-0 lg:grid-cols-3 lg:text-left">
            {products.map(({ id, title, image, description, price }) => (
              <li
                key={id}
                className="max-w-sm border border-gray-300 rounded-lg hover:shadow-lg"
              >
                <div>
                  <img
                    src={image}
                    alt={title}
                    className="w-full rounded-t-lg"
                  />
                  <div className="px-6 py-4">
                    <h3 className="font-bold text-xl mb-2">{title}</h3>
                    <p>{price}</p>
                    <p className="text-gray-700 text-base">{description}</p>
                  </div>
                  <div className="mb-4 flex justify-center w-full">
                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={() => {
                      initiateCheckout({
                        lineItems: [
                          {
                            price: id,
                            quantity: 1
                          }
                        ]
                      })
                    }}>
                      Buy now
                    </button>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </main>
    </>
  );
}
