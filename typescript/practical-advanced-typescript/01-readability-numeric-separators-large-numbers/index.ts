/**
 * The overall reading experience with this numeric separators is more pleasant,
 * but at the same time I find it difficult to maintain this with a lot of people working on the same project,
 * maybe with some eslint plugin it will be easier.
 */

class AmountInput {
  private static MAX_ALLOWED = 99_999_999;

  amount: number = 0;

  showTooltip() {
    // show tooltip
    setTimeout(() => {
      // hide tooltip
    }, 2_500);
  }

  formatMillion() {
    return this.amount / 1_000_000 + 'M';
  }
}
