interface IAnimal {
  age: number;
  eat(): void;
  speak(): string;
}

type AnimalTypeAlias = {
  age: number;
  eat(): void;
  speak(): string;
};

let animalInterface: IAnimal | undefined;
let animalTypeAlias: AnimalTypeAlias | undefined;

animalInterface = animalTypeAlias;

export {};
