type Pet = {
  pose(): void;
};

interface IFeline {
  nightvision: boolean;
}

interface ICat extends IFeline, Pet {}

type Cat = IFeline & Pet;

// you can implements a type and an interface in the same class
class HouseCat implements IFeline, Pet {
  pose(): void {
    throw new Error('Method not implemented.');
  }
  nightvision: boolean;
}

export {};
