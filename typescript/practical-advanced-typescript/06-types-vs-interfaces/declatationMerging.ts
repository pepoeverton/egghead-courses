/**
 *
 */
import * as $ from 'jquery';

$.fn.extend({
  hideChildren: function () {
    // ...
  },
});

$('test').hideChildren();

interface Foo {
  a: string;
}

interface Foo {
  b: string;
}

const foo: Foo = {
  a: 'We have a',
  b: 'We also have b',
};

console.log(foo);
