/**
 * Types and interfaces are quite similar, basically you can do almost the same thing with both.
 */

type Eat = (food: string) => void;
type AnimalList = string[];

interface IEat {
  (food: string): void;
}

interface IAnimalList {
  [index: number]: string;
}
