type IDog1 = {
  size: number;
};

type ICat1 = {
  color: string;
};

/**
 * The bigger difference between types and interfaces is that with types you can have a union with different types
 *
 */
type PetType = IDog1 | ICat1;

/**
 * An interface can only extend an object type or intersection of object types with statically known
 * So it's not allowed to have an interface that's extends an union.
 */
interface IPet extends PetType {}

/**
 * This error is because you need to be more specific about what kind of type or interface you're implementing
 * you cannot implement a class from a union
 */
class Pet implements PetType {}
