interface IPet {
  name: string;
  age: number;
  favoritePark?: string;
}

// Very useful for immutable things.
// "-?" this means that your removing all optionals in this new type
type ReadonlyPet = {
  readonly [K in keyof IPet]-?: IPet[K];
};

const pet: IPet = { name: 'Pretinha', age: 6 };
const readonlyPet: ReadonlyPet = {
  name: 'Pretinha',
  age: 6,
  favoritePark: 'Volkspark Friedrichshain', // This property is required as we removed all optionals from the original type
};

pet.age = 7;

// You cannot add a new value to the age property
readonlyPet.age = 7;
