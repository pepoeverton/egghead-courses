/**
 * strictNullChecks and strictPropertyInitialization: This flag will warn of any variable that is not being initialized.
 * Basically it's very useful and important to keep these flag enabled if you want to have a more consistent and safer code.
 */

class Library {
  titles!: string[]; // As this var is string[] it need to be initialized or it can also receive a definite assignment operator. (!)
  address: string = 'This is a test';
  isPublic: boolean;

  constructor() {
    this.isPublic = true;
  }
}

const library = new Library();

// This "if" here is because the initialization is not being done at the same time as the declaration. (Line 07)
if (library.titles) {
  const shortTitles = library.titles.filter((title) => title.length < 5);
  console.log('$shortTitles = ', shortTitles);
}
