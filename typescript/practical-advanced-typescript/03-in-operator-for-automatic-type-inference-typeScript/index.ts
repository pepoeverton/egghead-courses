/**
 * Basically the best way to check a type/interface in TS is using the "in" operator, it's simple and explicit.
 */

interface Admin {
  id: string;
  role: string;
}

interface User {
  email: string;
}

function redirect(user: Admin | User) {
  if ('role' in user) {
    routeToAdminPage(user.role);
  } else {
    routeToHomePage(user.email);
  }
}

function routeToAdminPage(role: string) {
  return role;
}

function routeToHomePage(email: string) {
  return email;
}
