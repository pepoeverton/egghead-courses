type Add = {
  type: 'Add';
  payload: string;
};

type RemoveOne = {
  type: 'Remove One';
  payload: number;
};

type RemoveAll = {
  type: 'Remove All';
};

// export class Add implements Action {
//   readonly type = 'Add';
//   constructor(public payload: string) {}
// }

// export class RemoveOne implements Action {
//   readonly type = 'Remove One';
//   constructor(public payload: number) {}
// }

// export class RemoveAll implements Action {
//   readonly type = 'Remove All';
// }

export type TodoActions = Add | RemoveOne | RemoveAll;
