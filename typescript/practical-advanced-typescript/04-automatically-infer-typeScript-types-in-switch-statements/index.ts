/**
 * Feature: Discriminated Unions
 * This is a good example of a inferring type in TS, since we have action.type as readonly and it's not a generic string type
 * we can make sure which type is the payload property without any casts or workaround.
 */

import { TodoActions } from './todo.actions';

interface ITodoState {
  todos: string[];
}

function todoReducer(
  action: TodoActions,
  state: ITodoState = { todos: [] }
): ITodoState {
  switch (action.type) {
    case 'Add': {
      return {
        todos: [...state.todos, action.payload],
      };
    }

    case 'Remove All': {
      return {
        todos: [],
      };
    }

    case 'Remove One': {
      return {
        todos: state.todos.slice().splice(action.payload, 1),
      };
    }
  }
}
