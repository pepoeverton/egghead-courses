interface TreeNode<T> {
  value: T;
  left: TreeNode<T>;
  right: TreeNode<T>;
}

interface LinkedListNode<T> {
  value: T;
  next: LinkedListNode<T>;
}

let node: LinkedListNode<string>;

interface Action {
  type: string;
}

// action1/state1 --> action2/state2 --> action3/state3

interface ListNode<T> {
  value: T;
  next: ListNode<T> | undefined;
  prev: ListNode<T> | undefined;
}

let action1 = { type: 'LOGIN' };
let action2 = { type: 'LOAD_POSTS' };

let actionNode1: ListNode<Action> = {
  value: action1,
  next: undefined,
  prev: undefined,
};

let actionNode2: ListNode<Action> = {
  value: action2,
  next: undefined,
  prev: actionNode1,
};

actionNode1.next = actionNode2;

let currentNode: ListNode<Action> | undefined = actionNode2;

do {
  console.log(currentNode.value);
  currentNode = currentNode.prev;
} while (currentNode);
